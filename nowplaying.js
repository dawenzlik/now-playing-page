/**
written by Daniel Wenzlik 2021 - https://danielwenzlik.com
Script under the GNU AGPLv3 license.
*/

var lastfmData = {
  baseURL:
    "https://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks&user=",
  // Your Last.fm Username
  user: "dani3300",
  // Your API key
  api_key: "a1fcf62b0656fae740fa2a43a10c5b85",
  additional: "&format=json&limit=1"
};

var getSetLastFM = function() {
  $.ajax({
    type: "GET",
    url:
      lastfmData.baseURL +
      lastfmData.user +
      "&api_key=" +
      lastfmData.api_key +
      lastfmData.additional,
    dataType: "json",
    success: function(resp) {
      var recentTrack = resp.recenttracks.track[0];
      var formatted =
        "<img src='https://scripts.danielwenzlik.com/np/title.png'>" + recentTrack.name;
      $("a#tracktitle")
        .html(formatted)
        .attr("href", recentTrack.url)
        .attr("title", recentTrack.name + " by " + recentTrack.artist["#text"])
        .attr("target", "_blank");

      var artistFormatted =
        "<img src='https://scripts.danielwenzlik.com/np/artist.png'>" +
        recentTrack.artist["#text"];
      $("a#trackartist")
        .html(artistFormatted)
        .attr("title", "Artist : " + recentTrack.artist["#text"]);
      $("img#trackart").attr("src", recentTrack.image[2]["#text"]);
    },
    error: function(resp) {
      $("a#tracktitle").html(
        "<img src='https://scripts.danielwenzlik.com/np/title.png'>" + "Silence!"
      );
      $("img#trackart").attr("src", "https://scripts.danielwenzlik.com/np/icon2022.png");
      var artistFormatted =
        "<img src='https://scripts.danielwenzlik.com/np/artist.png'>Dani3300";
      $("a#trackartist")
        .html(artistFormatted)
        .attr("href", "https://danielwenzlik.com/");
    }
  });
};

// Get the new one.
getSetLastFM();
// Start the countdown.
setInterval(getSetLastFM, 10 * 1000);
