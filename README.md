# Now-Playing-Page
Displays the current song you're listening to by using the Last.FM audioscrobbler.

This works with Tidal, Spotify & more streaming services. 

# Instructions:

1. Link your account - for example: Spotify => Last.FM.
2. Get an Last.FM API Key
3. Change the API Key in the nowplaying.js File along with your Username
4. Change the favorite Playlist with one of your own ones in the index.html
5. Make changes to the Design in the index.html and nowplayingstandalone.css
6. Upload everything to your webserver

# Demo:
You can view the Now Playing page here: https://danielwenzlik.com/nowplaying
